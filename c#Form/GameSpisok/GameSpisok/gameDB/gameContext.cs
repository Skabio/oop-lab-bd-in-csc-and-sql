﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace GameSpisok.gameDB
{
    public partial class gameContext : DbContext
    {
        public gameContext()
        {
        }

        public gameContext(DbContextOptions<gameContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Game> Game { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=127.0.0.1;uid=root;pwd=1111;database=game;port=4406");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>(entity =>
            {
                
                entity.ToTable("game");

                entity.HasIndex(e => e.Id)
                    .HasName("id")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ВремяПрохождения)
                    .HasColumnName("Время прохождения")
                    .HasMaxLength(100);

                entity.Property(e => e.ДатаВыхода)
                    .HasColumnName("Дата выхода")
                    .HasColumnType("date");

                entity.Property(e => e.Движок).HasMaxLength(100);

                entity.Property(e => e.Жанр).HasMaxLength(100);

                entity.Property(e => e.Название).HasMaxLength(100);

                entity.Property(e => e.ОценкаКритиков)
                    .HasColumnName("Оценка критиков")
                    .HasMaxLength(100);

                entity.Property(e => e.ОценкаПользователей)
                    .HasColumnName("Оценка пользователей")
                    .HasMaxLength(100);

                entity.Property(e => e.Платформа).HasMaxLength(100);

                entity.Property(e => e.Разработчик).HasMaxLength(100);

                entity.Property(e => e.РежимИгры)
                    .HasColumnName("Режим игры")
                    .HasMaxLength(255);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
