﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace GameSpisok.gameDB
{
    public partial class Game
    {
        [Key]
        public int Id { get; set; }
        public string Название { get; set; }
        public DateTime? ДатаВыхода { get; set; }
        public string Жанр { get; set; }
        public string Платформа { get; set; }
        public string Разработчик { get; set; }
        public string Движок { get; set; }
        public string РежимИгры { get; set; }
        public string ОценкаКритиков { get; set; }
        public string ОценкаПользователей { get; set; }
        public string ВремяПрохождения { get; set; }
    }
}
