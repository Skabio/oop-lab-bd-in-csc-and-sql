﻿using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameSpisok
{
    public partial class spisok : Form
    {
        public spisok()
        {
            InitializeComponent();
            context = new gameDB.gameContext();
            context.Game.Load();
            dataGridView1.DataSource = context.Game.Local.ToBindingList();
            dataGridView1.Columns["Id"].ReadOnly = true;
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        gameDB.gameContext context;

        private void button4_Click(object sender, EventArgs e)
        {
            Dobawit sp = new Dobawit();
            sp.Show();
            Hide();
        }

        Point lastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void button3_Click(object sender, EventArgs e)
        {

            string Message;
            Message = "Вы действительно хотите удалить строку?";

            if (MessageBox.Show(Message, "Внимание(Убедитесь,что выделили строку)", MessageBoxButtons.YesNo,MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)==DialogResult.No)
            {
                return;
            }
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            context.SaveChanges();
            MessageBox.Show("Список сохранен");
        }

    }
}
