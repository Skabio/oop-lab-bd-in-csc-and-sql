﻿using GameSpisok.gameDB;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GameSpisok
{
    public partial class Dobawit : Form
    {
        

        public Dobawit()
        {
            InitializeComponent();
            context = new gameDB.gameContext();
            context.Game.Load();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        gameDB.gameContext context;

        private void button3_Click(object sender, EventArgs e)
        {
            spisok sp = new spisok();
            sp.Show();
            Hide();
        }
        Point lastPoint;

        private void Dobawit_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void Dobawit_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

       
          private void button2_Click(object sender, EventArgs e)
          {

          context.Game.Add(new Game (){ Название = textBox1.Text , ДатаВыхода = dateTimePicker1.Value , Жанр = textBox3.Text 
          , Платформа = textBox4.Text, Разработчик = textBox5.Text, Движок = textBox6.Text, РежимИгры = textBox10.Text, 
           ОценкаКритиков = textBox9.Text , ОценкаПользователей = textBox8.Text , ВремяПрохождения = textBox7.Text } );

            context.SaveChanges();

            MessageBox.Show("Игра добавлена");

        }
       
    }
}
