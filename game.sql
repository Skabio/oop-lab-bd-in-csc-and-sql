-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3307
-- Время создания: Фев 24 2023 г., 13:20
-- Версия сервера: 5.7.24-log
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `game`
--

-- --------------------------------------------------------

--
-- Структура таблицы `game`
--

CREATE TABLE `game` (
  `id` int(10) UNSIGNED NOT NULL,
  `Название` varchar(100) NOT NULL,
  `Дата выхода` date NOT NULL,
  `Жанр` varchar(100) NOT NULL,
  `Платформа` varchar(100) NOT NULL,
  `Разработчик` varchar(100) NOT NULL,
  `Движок` varchar(100) NOT NULL,
  `Режим игры` varchar(255) NOT NULL,
  `Оценка критиков` int(100) NOT NULL,
  `Оценка пользователей` int(100) NOT NULL,
  `Время прохождения` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game`
--

INSERT INTO `game` (`id`, `Название`, `Дата выхода`, `Жанр`, `Платформа`, `Разработчик`, `Движок`, `Режим игры`, `Оценка критиков`, `Оценка пользователей`, `Время прохождения`) VALUES
(1, 'Cuphead', '2029-09-20', 'Платформер', 'Windows, Nintendo Switch, Xbox One, PlayStation 4, MacOS', 'Studio MDHR', 'Unity', 'Одиночная игра, Кооператив, RemotePlay', 9, 8, 11),
(2, 'Don\'t Starve Together', '2023-02-24', 'Выживание, Песочница', 'Windows, Linux, MacOS, Nintendo Switch', 'Klei Entertainment', 'Собственный движок', 'Одиночная игра, Кооператив, RemotePlay, PVP', 8, 9, 36),
(3, 'It takes two', '2026-03-20', 'Action-adventure', 'Windows, Xbox Series X/S, PlayStation 5, Nintendo Switch, Xbox One, PlayStation 4', 'Hazelight Studios', 'Unreal Engine 4', 'Кооператив', 9, 7, 14),
(4, 'Escape from Tarkov', '2005-11-20', 'Шутер, ММО, Ролевая игра', 'Windows', 'Battlestate Games', 'Unity', 'Массовая многопользовательская игра', 9, 4, 140),
(5, 'Propnight', '2001-12-20', 'Survival horror', 'Windows', 'Fntastic', 'Unreal Engine', 'Кооператив', 8, 10, 0),
(6, 'Fall Guys', '2009-06-20', 'Платформер, королевская битва', 'Windows, Xbox Series X/S, Nintendo Switch, Xbox One, PlayStation 4, PlayStation 5', 'Mediatonic', 'Unity', 'Многопользовательская', 8, 7, 0),
(7, 'Phasmophobia', '2018-09-20', 'Survival horror', 'Windows', 'Kinetic Games', 'Unity', 'Одиночная игра, Кооператив', 10, 10, 0),
(8, 'Minecraft', '2023-02-24', 'Песочница', 'Windows, Xbox 360, Android, PlayStation 3,  PlayStation 4', 'Mojang Studios', 'Bedrock Engine', 'Одиночная игра, Кооператив', 8, 7, 93),
(9, 'Among us', '2023-02-24', 'Инди', 'Windows, Android, Xbox Series X/S, PlayStation 5, Nintendo Switch, Xbox One, PlayStation 4, IOS', 'Innersloth', 'Unity', 'Одиночная игра, Кооператив', 8, 9, 0),
(10, 'The Past Within', '2002-11-20', 'Головоломка', 'Windows, MacOS, Android, iOS', 'Rusty Lake', 'Unity', 'Кооператив', 7, 9, 2),
(11, 'Project Zomboid', '2008-11-20', 'Survival horror', 'Windows, Java, Linux, MacOS', 'The Indie Stone', 'Собственный движок', 'Одиночная игра, Кооператив', 9, 9, 0),
(12, 'Astroneer', '2016-12-20', 'Приключение, Выживание', 'Windows, Nintendo Switch, Xbox One, PlayStation 4', 'System Era Softworks', 'Unreal Engine 4', 'Одиночная игра, Кооператив', 7, 9, 16),
(13, 'Ghost Watchers', '2028-07-20', 'Survival horror, Хоррор', 'Windows', 'Renderise', 'Unity', 'Одиночная игра, Кооператив', 8, 9, 0),
(14, 'The Forest', '2030-04-20', 'Survival horror', 'Windows, PlayStation 4', 'Endnight Games', 'Unity', 'Одиночная игра, Кооператив', 7, 8, 9),
(15, 'Escape the Backrooms', '2011-08-20', 'Хоррор, Головоломка', 'Windows', 'Fancy Games', 'Unreal Engine 5', 'Одиночная игра, Кооператив', 8, 9, 0),
(16, 'Dead by Daylight', '2014-06-20', 'Survival horror', 'Windows, Android, Xbox Series X/S, PlayStation 5, Nintendo Switch, Xbox One, PlayStation 4, IOS', 'Behaviour Interactive', 'Unreal Engine 4', 'Многопользовательскаяя', 6, 8, 0),
(17, 'Stardew Valley', '2026-02-20', 'Симулятор фермы', 'Windows, Android, Nintendo Switch, Xbox One, PlayStation 4, PlayStation Vita, IOS, Linux, MacOS', 'ConcernedApe', 'XNA', 'Одиночная игра, Кооператив', 9, 9, 53),
(18, 'Terraria', '2016-05-20', 'Платформер, Инди-игра', 'Windows, Xbox 360, Android, PlayStation 3, Xbox One', '505 Games Srl', 'XNA', 'Одиночная игра, Кооператив', 8, 9, 50),
(19, 'Ghost Exile', '2007-01-20', 'Survival horror', 'Windows', 'LostOneTeam', 'Unity', 'Одиночная игра, Кооператив', 8, 9, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `game`
--
ALTER TABLE `game`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `game`
--
ALTER TABLE `game`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
